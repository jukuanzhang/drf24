from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter ,SimpleRouter # 默认路由器,简单路由器

# urlpatterns = [
#     path('students/',StudentViewSet.as_view({'get':'list','post':'create'})),  # 请求：查询所有
#     path('students/<pk>/',StudentViewSet.as_view({'get':'retrieve','put':'update','delete':'destory'}))
# ]

urlpatterns = [
]

# 创建路由器
router = DefaultRouter()
# 注册路由
router.register('students', StudentViewSet)  # 学生
router.register('classes', ClassesViewSet)  # 班级
# 追加至接口路由列表当中
urlpatterns += router.urls

