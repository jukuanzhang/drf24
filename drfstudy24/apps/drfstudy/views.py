from django.shortcuts import render
from rest_framework import viewsets  # 视图集类
from rest_framework.response import Response  # 响应
from .models import *  # 模型类
from .serializers import *  # 序列化器类
from rest_framework.status import HTTP_404_NOT_FOUND,HTTP_201_CREATED,HTTP_204_NO_CONTENT
from rest_framework.decorators import action  # 行为路由器

# Create your views here.
"""
    视图集：封装了一套响应式方法
    定义：
        create创建数据，list查询所有，retrieve单行查询，update修改，destory删除
"""

# 学生的视图集
# class StudentViewSet(viewsets.ViewSet):
#     # 查询所有
#     def list(self,request):
#         students = Student.objects.all()  # 查询所有 查询集
#         # 序列化操作 查询集==>json
#         serializer = StudentSerializer(students,many=True)
#         return Response(serializer.data)  # 数据
#
#     # 创建 写入
#     def create(self,request):
#         # 反序列化 json==>模型
#         serializer = StudentSerializer(data=request.data)  # 前端传过来的表单、json 数据
#         # 校验是否符合模型数据
#         serializer.is_valid(raise_exception=True)
#         # 保存
#         serializer.save()
#         # 返回
#         return Response(serializer.data)
#
#     # 单行查询
#     def retrieve(self,request, pk):  # 主键id的值
#         try:
#             students = Student.objects.get(id=pk)   # 报错
#         except Student.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#
#         # 模型==>json 前端 序列化
#         serializer = StudentSerializer(students)
#         return Response(serializer.data)  # 单条数据
#
#     # 修改 update
#     def update(self,request,pk):
#         try:
#             students = Student.objects.get(id=pk)   # 报错
#         except Student.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#
#         # 反序列化 json==>模型 修改  源模型数据，前端传过来的数据
#         serializer = StudentSerializer(students,data=request.data)  # 前端传过来的表单、json 数据
#         # 校验是否符合模型数据
#         serializer.is_valid(raise_exception=True)
#         # 保存
#         serializer.save()
#         # 返回
#         return Response(serializer.data)
#
#     # destory删除
#     def destory(self, request, pk):
#         try:
#             students = Student.objects.get(id=pk)   # 报错
#         except Student.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#
#         # 删除 重写
#         students.delete()
#         return Response(status=HTTP_204_NO_CONTENT)

# 学生视图集
class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.filter(is_delete=False)  # 查询的是逻辑删除为False的值，未被删除的值
    serializer_class = StudentSerializer  # 指定使用的序列化器

# 班级视图集
class ClassesViewSet(viewsets.ModelViewSet):
    queryset = Classes.objects.filter(is_delete=False)  # 查询的是逻辑删除为False的值，未被删除的值
    serializer_class = ClassesSerializer  # 指定使用的序列化器

    # 行为方法
    @action(methods=['get'],detail=False)
    def last(self,request):
        classes = Classes.objects.last()  # 班级当中最后一行数据
        # 序列化  查询集==>json
        return Response(self.get_serializer(classes).data)


