from rest_framework import serializers  # 序列化器类
from .models import *  # 模型类


# 学生的序列化器  序列化和反序列化
# 序列化器最好是模型类名字+Serializer
# class StudentSerializer(serializers.Serializer):
# id = serializers.CharField(label='ID',read_only=True)  # 只读
# name = serializers.CharField(max_length=30,label='姓名')  # 最大输入长度为30
# age = serializers.IntegerField(label='年龄',required=False)  # 年龄可给可不给
# sex = serializers.IntegerField(label='性别',required=False)  # 性别可给可不给
# create_time = serializers.DateTimeField(label='创建时间', required=False)
# update_time = serializers.DateTimeField(label='更新时间', required=False)
# is_delete = serializers.BooleanField(default=False, label='逻辑删除', required=False)
#
# # 创建，写入模型数据
# def create(self, validated_data):  # 字典，表单数据 {name": "zl","age": 18,"sex": 1}
#     return Student.objects.create(**validated_data)  # name='zl',age=18
#
# # 修改  源模型数据，前端传过来的数据
# def update(self, instance, validated_data):
#     instance.name = validated_data.get('name',instance.name)
#     instance.age = validated_data.get('age',instance.age)
#     instance.sex = validated_data.get('sex',instance.sex)
#     instance.save()
#     return instance

class StudentSerializer(serializers.ModelSerializer):
    class_name = serializers.CharField(source='classes.name',read_only=True)  # 引用其它模型中某个字段的值

    class Meta:
        model = Student  # 模型类==>序列化器字段
        # 指定映射哪些字段
        # fields = '__all__'
        # fields = ["name","age","sex"]
        # 不想展示某些字段
        # 不想展示某些字段
        exclude = ['is_delete']

        # 数据校验 age 年龄范围0-100之间
        extra_kwargs = {
            'age': {'min_value': 0, 'max_value': 100}
        }
    # 属性级别校验 name
    # def validate_name(self, value):  # '子冧'
    #     if 'mq' not in value.lower():
    #         raise serializers.ValidationError('用户名需要包含mq字符')
    #     return value

    # 对象级别校验
    def validate(self, data):  # 获取所有表单值 对象 字典 {'name':'zl', 'age':18, 'sex':1}

        if 'mq' not in data['name'].lower():
            raise serializers.ValidationError('用户名需要包含mq字符')

        if data['age'] < 18:
            raise serializers.ValidationError('未成年人禁止访问！')

        return data

# 学生的序列化器2
class StudentSerializerSimple(serializers.ModelSerializer):

    class Meta:
        model = Student  # 模型类==>序列化器字段
        # 指定映射哪些字段
        # fields = '__all__'
        fields = ["id","name","age","sex"]

# 班级的序列化器
class ClassesSerializer(serializers.ModelSerializer):
    # 新增字段，嵌套序列化，所有的数据
    student_set = StudentSerializerSimple(many=True,read_only=True)

    class Meta:
        model = Classes  # 模型类==>序列化器字段
        # 指定映射哪些字段
        # fields = '__all__'
        # fields = ["name","age","sex"]
        # 不想展示某些字段
        # 不想展示某些字段
        exclude = ['is_delete']